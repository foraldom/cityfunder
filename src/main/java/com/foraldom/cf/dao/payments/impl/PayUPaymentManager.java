/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.payments.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.service.dao.api.ContributionManager;
import com.foraldom.cf.model.Contribution;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import com.foraldom.cf.service.payments.api.PaymentsManager;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class PayUPaymentManager implements PaymentsManager {

    private final int POS_ID = 145227;
    private final String SECOND_KEY = "13a980d4f851f3d9a1cfc792fb1f5e50";
    private final String CLIENT_SECRET = "12f071174cb7eb79d4aac5bc2f07563f";

    private String OAuthAccessToken = null;
    private String OAuthRefreshToken = null;

    public PayUPaymentManager() throws Exception {
          setUpPayments();
    }

    @Inject
    private ContributionManager contributionManager;

    private void setUpPayments() throws Exception {
        Client client = ClientBuilder.newClient();
        Entity<String> payload = Entity.text("grant_type=client_credentials&#38;client_id=" + POS_ID + "&#38;client_secret=" + CLIENT_SECRET);
        Response response = client.target("https://private-cd9569-payu21.apiary-mock.com/pl/standard/user/oauth/authorize")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .post(payload);

        if (response.getStatus() != HttpServletResponse.SC_OK) {
            throw new Exception("Couldn't retrieve access token");
        }
        HashMap<String, String> responseMap = new ObjectMapper().readValue(response.readEntity(String.class), HashMap.class);
        OAuthAccessToken = responseMap.get("access_token");
        OAuthRefreshToken = responseMap.get("refresh_token");
    }

    @Override
    public Response createPayment(int price, Project project, String userIp, User u) {
        
        Client client = ClientBuilder.newClient();
        Entity payload = Entity.json(""
                + "{  'notifyUrl': 'https://your.eshop.com/notify',  "
                + "'customerIp': '" + userIp + "',  "
                + "'merchantPosId': '" + POS_ID + "',  "
                + "'description': 'Cityfunder',  "
                + "'currencyCode': 'CZK',  "
                + "'totalAmount': '" + price + "',  "
                + "'products': [    "
                + "{"
                + "'name': 'Finanční příspěvek na projekt " + project.getName() + "',"
                + "'unitPrice': '" + price + "',"
                + "'quantity': '1'"
                + "}"
                + "]"
                + "}");
        Response payUResponse = client.target("https://private-cd9569-payu21.apiary-mock.com/api/v2_1/orders/")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Bearer " + OAuthAccessToken)
                .post(payload);
        HashMap<String, Object> responseMap;
        try {
            String responseEntity = payUResponse.readEntity(String.class);

            // mock response 
//            String responseEntity = "{\"orderId\": \"9QGM1F41NX170411GUEST000P01\", "
//                    + "\"status\": { \"statusCode\": \"SUCCESS\"},"
//                    + "\"redirectUri\": \"https://secure.payu.com/pl/standard/co/summary?sessionId=amyNgNIdvqIKVuiO1GQ3Za2vdwG0USxK&merchantPosId=145227&timeStamp=1491898420065&showLoginDialog=false&apiToken=795fb7115fed979e79e0838181208890f3ec1be957e5fc860b24c11469ccf1fb\"}";
            responseEntity = responseEntity.replace("\"SUCCESS\",", "\"SUCCESS\""); // response bug fix :(
            responseMap = new ObjectMapper().readValue(responseEntity, HashMap.class);
        } catch (IOException ex) {
            Logger.getLogger(PayUPaymentManager.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }

        if (!((HashMap) responseMap.get("status")).get("statusCode").equals("SUCCESS")) {
            return Response.serverError().entity("Error occured while creating order").build();
        }

        Contribution c = new Contribution();
        c.setContribValue(price);
        c.setContributed(new Date());
        c.setContributor(u);
        c.setContributedProject(project);
        c.setContributionState(Contribution.ContributionState.WAITING);
        c.setOrderId(responseMap.get("orderId").toString());
        contributionManager.createContribution(c);
        return Response.status(Response.Status.FOUND).header("Location", responseMap.get("redirectUri")).build();
    }

    @Override
    public boolean recieveOrderNotification(HttpServletRequest request) {
        String requestBody = readRequestBody(request);
        
        if (!verifyNotificationSignature(request, requestBody)) {
            return false;
        }
        
        HashMap orderInfo = extractOrderInfo(requestBody);
        switch(orderInfo.get("status").toString()){
            case "COMPLETED": 
                changeContributionToPaid(orderInfo.get("orderId").toString());
                break;
            default :
                throw new UnsupportedOperationException("Only COMPLETED notification implemented");
        }
        return true;
    }

    private String readRequestBody(HttpServletRequest request) {
        try {
            char[] requestBuffer = new char[10000];
            request.getReader().read(requestBuffer);
            return new String(requestBuffer);
        } catch (IOException ex) {
            Logger.getLogger(PayUPaymentManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private HashMap extractOrderInfo(String jsonMessage) {
        try {
            ObjectMapper om = new ObjectMapper();
            HashMap<String, Object> messageMap = om.readValue(jsonMessage, HashMap.class);
            return (HashMap) messageMap.get("order");
        } catch (IOException ex) {
            Logger.getLogger(PayUPaymentManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private boolean verifyNotificationSignature(HttpServletRequest request, String requestBody) {
        try {
            String signatureHeader = request.getHeader("OpenPayu-Signature");
            if (signatureHeader == null) {
                return false;
            }
            String[] headerParts = signatureHeader.split(";");

            String signature;
            String algorithm;
            if (headerParts.length == 4) {
                signature = headerParts[1].split("=")[1];
                algorithm = headerParts[2].split("=")[1];
            } else {
                return false;
            }

            byte[] bytesOfMessage = (requestBody + SECOND_KEY).getBytes("UTF-8");

            MessageDigest md;
            try {
                md = MessageDigest.getInstance(algorithm);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(PayUPaymentManager.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < thedigest.length; ++i) {
                sb.append(Integer.toHexString((thedigest[i] & 0xFF) | 0x100).substring(1, 3));
            }
            String hash = sb.toString();
            return hash.equals(signature);
        } catch (IOException ex) {
            Logger.getLogger(PayUPaymentManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public int getSuccessPaymentsSum(Project project) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void changeContributionToPaid(String orderId) {
        contributionManager.changeContributionState(orderId, Contribution.ContributionState.PAID);
    }

}
