/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.ContributionManager;
import com.foraldom.cf.service.dao.api.ProjectManager;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Contribution;
import com.foraldom.cf.model.Project;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@RequestScoped
@Named
public class ContributionManagerJPA extends AbstractJPAManager<Contribution> implements ContributionManager {
    
    @Inject
    private ProjectManager projectManager;

    public ContributionManagerJPA() {
        super(Contribution.class);
    }

    @Override
    public void createContribution(Contribution c) {
        try {
            super.create(c);
        } catch (Exception ex) {
            Logger.getLogger(ContributionManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean changeContributionState(String orderId, Contribution.ContributionState state) {
        Contribution c = findContribution(orderId);
        if(c == null){
            return false;
        }
        c.setContributionState(state);
        update(c);      
        return true;
    }

    @Override
    public Contribution findContribution(String orderId) {
        return super.findEntityByParam("orderId", orderId);
    }

    @Override
    public void update(Contribution c) {
        super.updateEntity(c);
        updateProjectPledgedProgress(c.getContributedProject().getId());
    }
    
    

    @Override
    public List<Contribution> findProjectContributions(Long projectId) {
       return super.findEntitiesByParam("contributedProject.id", projectId);
    }

    @Override
    public Long getSuccessProjectContributionsSum(Long projectId) {
         EntityManager em = emf.createEntityManager();
         Query q = em.createQuery("SELECT sum(c.contribValue) from Contribution c WHERE c.contributedProject.id = :projectid and c.contributionState = :state");
         return (Long) q.setParameter("projectid", projectId).setParameter("state", Contribution.ContributionState.PAID).getSingleResult();
    }
    
    public void updateProjectPledgedProgress(Long projectId){
        
        Project p = projectManager.findProject(projectId);
        if(p.getProgress() == null){
            // TODO: only when project has 0 financial costs, no need to contribute.....
            p.setProgress(new Project.ProjectProgress());
            p.getProgress().setGoal(0L);
        }     
        Long sum = getSuccessProjectContributionsSum(projectId);
        p.getProgress().setPledged(sum);
        try {
            projectManager.edit(p);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(ContributionManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ContributionManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //System.out.println("updating pledged in project " + projectId + " to " + sum );
    }

    @Override
    public List<Contribution> findPaidProjectContributions(Long projectId) {
        TypedQuery<Contribution> query = getEntityManager().createQuery("SELECT c FROM Contribution c Where c.contributionState = :contributionState and c.contributedProject.id = :projectId", Contribution.class);
        return query.setParameter("projectId", projectId).setParameter("contributionState", Contribution.ContributionState.PAID).getResultList();
    }

    @Override
    public Contribution findContribution(Long contributionId) {
        return super.findEntityByParam("id", contributionId);
    }

}
