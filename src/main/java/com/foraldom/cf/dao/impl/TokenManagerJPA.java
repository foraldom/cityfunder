/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.foraldom.cf.service.dao.api.TokenManager;
import com.foraldom.cf.model.Token;
import com.foraldom.cf.model.User;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.NewCookie;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class TokenManagerJPA implements TokenManager {

    private final String SECRET = "5kO2cN1L+niz4NK3qiu+ueAe7B4yJBvh8xNUWzl0";

    @PersistenceUnit
    protected EntityManagerFactory emf;

    @Resource
    protected UserTransaction userTransaction;

    @Override
    public void create(Token token) {
        try {
            userTransaction.begin();
            EntityManager em = emf.createEntityManager();
            em.persist(token);
            em.close();
            userTransaction.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException | NotSupportedException ex) {
            Logger.getLogger(TokenManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isValid(String tokenId) {
        EntityManager em = emf.createEntityManager();
        try {
            Token foundToken = find(tokenId);
            return (foundToken != null && foundToken.isValid());
        } finally {
            em.close();
        }
    }

    @Override
    public Token find(String tokenId) {
        EntityManager em = emf.createEntityManager();
        try {
            return em.find(Token.class, tokenId);
        } finally {
            em.close();
        }
    }

    @Override
    public String createJWT(Date expires, User u) {
        try {
            return JWT.create()
                    .withIssuer("cityfunder-backendapp")
                    .withExpiresAt(expires)
                    .withClaim("username", u.getName())
                    .withClaim("role", u.getUserRole().toString())
                    .withClaim("userid", u.getId().intValue())
                    .sign(Algorithm.HMAC256(SECRET));
        } catch (IllegalArgumentException | UnsupportedEncodingException ex) {
            Logger.getLogger(TokenManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public DecodedJWT decodeJWT(String JWTString) {
        Algorithm algorithm = null;
        try {
            algorithm = Algorithm.HMAC256(SECRET);
        } catch (IllegalArgumentException | UnsupportedEncodingException ex) {
            Logger.getLogger(TokenManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("cityfunder-backendapp")
                .build();
        return verifier.verify(JWTString);
    }

    @Override
    public void createFromCookie(NewCookie tokenCookie, User u) {
        Token t = new Token();
        t.setId(tokenCookie.getValue());
        t.setExpires(tokenCookie.getMaxAge());
        t.setOwner(u);
        create(t);
    }

    @Override
    public void delete(String tokenId) {
        try {
            Token token = find(tokenId);
            userTransaction.begin();
            EntityManager em = emf.createEntityManager();
            em.remove(em.merge(token));
            em.close();
            userTransaction.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException | NotSupportedException ex) {
            Logger.getLogger(TokenManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
    
}
