/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.ReactionManager;
import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.PreexistingEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.Reaction;
import com.foraldom.cf.model.User;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class ReactionManagerJPA implements Serializable, ReactionManager {

    @PersistenceUnit
    protected EntityManagerFactory emf;

    @Resource
    protected UserTransaction utx;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void create(Reaction reaction) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(reaction);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findReaction(reaction.getUser(), reaction.getProject()) != null) {
                throw new PreexistingEntityException("Reaction " + reaction + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Reaction> getProjectReactions(Long projectId, Reaction.ReactionType reactionType) {

        EntityManager em = getEntityManager();
        try {
            if (reactionType == null) {
                TypedQuery<Reaction> query = em.createQuery("SELECT r FROM Reaction r Where r.project.id = :projectId", Reaction.class);
                return query.setParameter("projectId", projectId).getResultList();
            }
            TypedQuery<Reaction> query = em.createQuery("SELECT r FROM Reaction r Where r.project.id = :projectId and r.reaction = :reactionType", Reaction.class);
            return query.setParameter("projectId", projectId).setParameter("reactionType", reactionType).getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public Long getProjectReactionsCount(Long projectId, Reaction.ReactionType reactionType) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT count(r) FROM Reaction r Where r.project.id = :projectId and r.reaction = :reactionType");
            return (Long) query.setParameter("projectId", projectId).setParameter("reactionType", reactionType).getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public boolean remove(Reaction reaction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param reaction
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    @Override
    public void edit(Reaction reaction) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            reaction = em.merge(reaction);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                User id = reaction.getUser();
                if (findReaction(id, reaction.getProject()) == null) {
                    throw new NonexistentEntityException("The reaction with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void destroy(Reaction reaction) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Reaction r;
            r = findReaction(reaction.getUser(), reaction.getProject());
            if (r == null) {
                throw new NonexistentEntityException("Reaction doesnt exists");
            }
            em.remove(em.merge(r));
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reaction> findReactionEntities() {
        return findReactionEntities(true, -1, -1);
    }

    public List<Reaction> findReactionEntities(int maxResults, int firstResult) {
        return findReactionEntities(false, maxResults, firstResult);
    }

    private List<Reaction> findReactionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery<Reaction> cq = em.getCriteriaBuilder().createQuery(Reaction.class);
            cq.select(cq.from(Reaction.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public int getReactionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery<Long> cq = em.getCriteriaBuilder().createQuery(Long.class);
            Root<Reaction> rt = cq.from(Reaction.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Reaction findReaction(User user, Project project) {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Reaction> query = em.createQuery("SELECT r FROM Reaction r Where r.project = :project and r.user = :user", Reaction.class);
            Reaction r = query.setParameter("project", project).setParameter("user", user).getSingleResult();
            return r;
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

}
