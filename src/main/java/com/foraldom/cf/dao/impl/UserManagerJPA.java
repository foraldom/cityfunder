/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.UserManager;
import com.foraldom.cf.model.User;
import com.foraldom.cf.utils.api.PasswordHash;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class UserManagerJPA extends AbstractJPAManager<User> implements UserManager {

    @Inject
    PasswordHash psswdHash;

    public UserManagerJPA() {
        super(User.class);
    }

    @Override
    public boolean registerUser(User user) {
        if (findUserByParam("email", user.getEmail()) != null) {
            return false;
        }
        if (user.getUserRole().equals(User.UserRole.REGISTERED)) {
            try {
                user.setPassword(psswdHash.createHash(user.getPassword()));
                super.create(user);
                return true;
            } catch (Exception ex) {
                Logger.getLogger(UserManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public User findUser(String email) {
        return super.findEntityByParam("email", email);
    }

    @Override
    public User findUserByParam(String paramName, Object paramValue) {
        return super.findEntityByParam(paramName, paramValue);
    }

    @Override
    public boolean verifyUser(String email, String password) {
        User u = findUser(email);
        return (u != null && psswdHash.checkPsswd(password, u.getPassword()));
    }

    @Override
    public boolean updateUser(User u) {
        return super.updateEntity(u);
    }

    @Override
    public boolean updateUserPsswd(User u, String newPassword) {
        u.setPassword(psswdHash.createHash(newPassword));
        return updateUser(u);
    }
}
