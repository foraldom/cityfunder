/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.AbstractEntity;
import com.foraldom.cf.utils.impl.BeanMerger;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.beanutils.BeanUtilsBean;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 * @param <T>
 */
public abstract class AbstractJPAManager<T extends AbstractEntity> {

    private final Class<T> entityClass;

    @PersistenceUnit
    protected EntityManagerFactory emf;

    @Resource
    protected UserTransaction utx;
    
    

    protected EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public AbstractJPAManager(Class<T> entityClass) {
        this.entityClass = entityClass;
        
    }

    public T findEntityByParam(String paramName, Object paramValue) {
        EntityManager em = emf.createEntityManager();
        try {
            String queryParamName = paramName.replace(".", "");
            TypedQuery<T> query = em.createQuery("SELECT u FROM " + entityClass.getName() + " u Where u." + paramName + " = :" + queryParamName, entityClass);
            return query.setParameter(queryParamName, paramValue).getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    public List<T> findEntitiesByParam(String paramName, Object paramValue) {
        EntityManager em = emf.createEntityManager();
        try {
            String queryParamName = paramName.replace(".", "");
            TypedQuery<T> query = em.createQuery("SELECT u FROM " + entityClass.getName() + " u Where u." + paramName + " = :" + queryParamName, entityClass);
            return query.setParameter(queryParamName, paramValue).getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    public List<T> findEntities() {
        return findEntities(true, 0, 0);
    }

    public List<T> findEntities(int maxResults, int firstResult) {
        return findEntities(false, maxResults, firstResult);
    }

    private List<T> findEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(entityClass);
            cq.select(cq.from(entityClass));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public void create(T entity) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(entity);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public boolean updateEntity(T entity) {
        try {
            T origEntity = findEntityByParam("id", entity.getId());
            BeanUtilsBean merger = new BeanMerger();
            merger.copyProperties(origEntity, entity);
            utx.begin();
            EntityManager em = emf.createEntityManager();
            em.persist(em.merge(origEntity));
            em.close();
            utx.commit();
            return true;
        } catch (IllegalAccessException | InvocationTargetException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException | NotSupportedException ex) {
            Logger.getLogger(UserManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void destroy(T entity) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            T r;
            r = findEntityByParam("id", entity.getId());
            if (r == null) {
                throw new NonexistentEntityException("Reaction doesnt exists");
            }
            em.remove(em.merge(r));
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
