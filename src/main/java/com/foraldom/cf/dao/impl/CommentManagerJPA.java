/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.CommentManager;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Comment;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class CommentManagerJPA extends AbstractJPAManager<Comment> implements CommentManager {

    public CommentManagerJPA() {
        super(Comment.class);
    }
    
    @Override
    public void create(Comment comment) throws RollbackFailureException, Exception{
        super.create(comment);
    }

    @Override
    public List<Comment> getProjectComments(long projectId) {
        return super.findEntitiesByParam("project.id", projectId);
    }

    @Override
    public Comment findComment(Long commentId) {
        return super.findEntityByParam("id", commentId);
    }

    @Override
    public void edit(Comment comment) {
        super.updateEntity(comment);
    }

    @Override
    public void destroy(Comment c) throws RollbackFailureException, Exception {
        super.destroy(c);
    }
    
    
    
    

}
