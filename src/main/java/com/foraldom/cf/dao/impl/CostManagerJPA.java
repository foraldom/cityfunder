/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.CostManager;
import com.foraldom.cf.service.dao.api.ProjectManager;
import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Cost;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.Project.ProjectProgress;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class CostManagerJPA extends AbstractJPAManager<Cost> implements CostManager {
    
    @Inject
    ProjectManager projectManager;

    public CostManagerJPA() {
        super(Cost.class);
    }

    @Override
    public void create(Cost cost) throws RollbackFailureException, Exception {
        super.create(cost);
        updateProjectProgress(cost.getProject().getId());
    }

    @Override
    public List<Cost> getProjectCosts(long projectId) {
        return super.findEntitiesByParam("project.id", projectId);
    }

    @Override
    public void edit(Cost cost) throws NonexistentEntityException, com.foraldom.cf.service.dao.exceptions.RollbackFailureException, Exception {
        super.updateEntity(cost);
        Cost c = findCost(cost.getId());
        updateProjectProgress(c.getProject().getId());
    }

    @Override
    public void destroy(Cost c) throws RollbackFailureException, Exception {
        super.destroy(c);
        updateProjectProgress(c.getProject().getId());
    }

    @Override
    public Cost findCost(Long costId) {
        return super.findEntityByParam("id", costId);
    }
    
    @Override
    public void updateProjectProgress(Long projectId){
        List<Cost> projectCosts = getProjectCosts(projectId);
        
        Long projectFinanceGoal = 0L;
        for (Cost c: projectCosts){
            if(c.getCostType().equals(Cost.CostType.FINANCE)){
                projectFinanceGoal += c.getCostAmount();
            }
        }
        
        Project p = projectManager.findProject(projectId);
        if(p.getProgress() == null){
            p.setProgress(new ProjectProgress());
            p.getProgress().setPledged(0L);
        }        
        p.getProgress().setGoal(projectFinanceGoal);
        try {
            projectManager.edit(p);
        } catch (com.foraldom.cf.service.dao.exceptions.RollbackFailureException ex) {
            Logger.getLogger(CostManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CostManagerJPA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
