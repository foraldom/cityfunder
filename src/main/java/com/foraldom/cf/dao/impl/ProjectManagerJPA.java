/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.CostManager;
import com.foraldom.cf.service.dao.api.ProjectManager;
import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Permission;
import com.foraldom.cf.model.ProjectPermission;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class ProjectManagerJPA extends AbstractJPAManager<Project> implements ProjectManager {
    
    public ProjectManagerJPA() {
        super(Project.class);
    }

    @Override
    public void create(Project project) throws RollbackFailureException, Exception {
        super.create(project);
    }

    @Override
    public Project findProject(Long id) {
        return super.findEntityByParam("id", id);
    }

    @Override
    public List<Project> findProjectEntities() {
        return super.findEntities();
    }

    @Override
    public void edit(Project project) throws NonexistentEntityException, RollbackFailureException, Exception {
        super.updateEntity(project);
    }

    @Override
    public List<Project> findPublicProjects() {
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<Project> query = em.createQuery("SELECT p FROM Project p Where p.projectState != :s1 and p.projectState != :s2 and p.projectState != :s3", Project.class);
            return query.setParameter("s1", Project.ProjectState.CANCELED).setParameter("s2", Project.ProjectState.NEW).setParameter("s3", Project.ProjectState.FOR_APPROVAL).getResultList();
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public List<Project> findProjectsByState(String state) {
        return super.findEntitiesByParam("projectState", Project.ProjectState.valueOf(state.toUpperCase()));
    }

    @Override
    public List<Project> findUserProjects(User u, boolean findOnlyPublicProjects) {
        EntityManager em = emf.createEntityManager();
        try {
            if (findOnlyPublicProjects) {
                TypedQuery<Project> query = em.createQuery("SELECT p FROM Project p Where p.projectState != :s1 and p.projectState != :s2 and p.projectState != :s3 and p.author.id = :uid", Project.class);
                return query
                        .setParameter("s1", Project.ProjectState.CANCELED)
                        .setParameter("s2", Project.ProjectState.NEW)
                        .setParameter("s3", Project.ProjectState.FOR_APPROVAL)
                        .setParameter("uid", u.getId())
                        .getResultList();
            }
            else {
                return findEntitiesByParam("author.id", u.getId());
            }
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

}
