/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.dao.impl;

import com.foraldom.cf.service.dao.api.NewsManager;
import com.foraldom.cf.service.dao.exceptions.PreexistingEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.News;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class NewsManagerJPA extends AbstractJPAManager<News> implements NewsManager {

    public NewsManagerJPA() {
        super(News.class);
    }

    @Override
    public News findNews(Long id) {
        return super.findEntityByParam("id", id);
    }

    
    @Override
    public void create(News news) throws PreexistingEntityException, RollbackFailureException, Exception {
        super.create(news);
    }

    @Override
    public List<News> getProjectNews(long projectId) {
        return super.findEntitiesByParam("project.id", projectId);
    }
    

}
