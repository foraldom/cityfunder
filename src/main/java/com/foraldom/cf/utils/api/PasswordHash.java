/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.utils.api;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */

public interface PasswordHash {
    public String createHash(String plainPsswd);
    public boolean checkPsswd(String plainPsswd, String hashedPsswd);
}
