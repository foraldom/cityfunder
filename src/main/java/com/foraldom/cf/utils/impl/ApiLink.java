/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.utils.impl;

import java.io.Serializable;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public class ApiLink implements Serializable {

    public ApiLink() {
    }
    
    public ApiLink(String rel, String url) {
        this.rel = rel;
        this.href = url;
    }
    
    public String rel;
    public String href;

}
