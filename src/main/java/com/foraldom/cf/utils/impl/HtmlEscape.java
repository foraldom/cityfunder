/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.utils.impl;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public class HtmlEscape {
    
    public static String escape(String text){
        String unescapedText = unescapeHtml4(text);
        return escapeHtml4(unescapedText);
    }

}
