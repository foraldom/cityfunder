/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.utils.impl;

import com.foraldom.cf.utils.api.PasswordHash;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.mindrot.jbcrypt.BCrypt;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Named
@RequestScoped
public class BCryptHash implements PasswordHash {
    
    @Override
    public String createHash(String plainPsswd) {
        return BCrypt.hashpw(plainPsswd, BCrypt.gensalt());
    }

    @Override
    public boolean checkPsswd(String plainPsswd, String hashedPsswd) {
        return BCrypt.checkpw(plainPsswd, hashedPsswd);
    }

}
