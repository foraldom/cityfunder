/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.service;


import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import com.foraldom.cf.utils.impl.ApiLink;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Path("/")
public class ApiRoot {
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response apiRoot(){
        HashMap<String, Object> apiRootData = new HashMap<>();
        
        List<ApiLink> links = new ArrayList<>();
        links.add(new ApiLink("users", "/" + User.URL_PART));
        links.add(new ApiLink("projects", "/" + Project.URL_PART));
        links.add(new ApiLink("authentication", "/auth" ));
        
        apiRootData.put("links", links);
        apiRootData.put("name", "CityFunder API");
        apiRootData.put("version", "1.0");
        
        return Response.ok(apiRootData).build();
    }
    
}
