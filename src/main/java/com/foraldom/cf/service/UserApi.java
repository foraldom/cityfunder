/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.foraldom.cf.service.dao.api.ProjectManager;
import com.foraldom.cf.service.dao.api.UserManager;
import com.foraldom.cf.model.User;
import com.foraldom.cf.utils.api.PATCH;
import com.foraldom.cf.utils.impl.EntityViews;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Cookie;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Path("users")
public class UserApi extends AbstractApi {

    @XmlRootElement
    public static class NewPsswdRequest {

        public String newPassword;
        public String oldPassword;
    }

    @Inject
    UserManager userManager;

    @Inject
    ProjectManager projectManager;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response register(User u) {
        u.setUserRole(User.UserRole.REGISTERED); // only for public registration
        if (userManager.registerUser(u)) {
            try {
                return Response
                        .created(new URI("/users/" + u.getId()))
                        .entity(createJsonWithView(EntityViews.Public.class, u))
                        .build();

            } catch (JsonProcessingException | URISyntaxException ex) {
                Logger.getLogger(UserApi.class.getName()).log(Level.SEVERE, null, ex);
                return Response.serverError().build();
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(buildErrorJsonMessage("User with this email (" + u.getEmail() + ") already exists.")).build();
        }
    }

    @GET
    @Path("/current")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCurrentUser(@CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            User u = userManager.findUserByParam("id",getUserId(cookie));
            if (u == null) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            return Response.ok(createJsonWithView(EntityViews.Private.class, u)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(UserApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{user_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProfile(@PathParam("user_id") long userId, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            User u = userManager.findUserByParam("id", userId);
            if (u == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(createJsonWithView(EntityViews.Public.class, u)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(UserApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @PATCH
    @Path("/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateUser(@CookieParam("accessTokenCookie") Cookie cookie, @PathParam("user_id") Long userId, User u) {
        if (authorizeUser(cookie, User.UserRole.REGISTERED) && getUserId(cookie).equals(userId)) {
            if (u.getEmail()!= null || u.getPassword() != null || u.getUserRole() != null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(buildErrorJsonMessage("cant change email, password or role!")).build();
            }
            u.setId(userId);
            if (userManager.updateUser(u)) {
                return Response.noContent().build();
            } else {
                return Response.serverError().build();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).entity("user not authorized").build();
        }
    }

    @PUT
    @Path("/{user_id}/password")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response changePassword(@CookieParam("accessTokenCookie") Cookie cookie, NewPsswdRequest request) {
        if (authorizeUser(cookie, User.UserRole.REGISTERED)) {
            
            User u = userManager.findUserByParam("id",getUserId(cookie));
            if(!userManager.verifyUser(u.getEmail(), request.oldPassword)){
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            if (userManager.updateUserPsswd(u, request.newPassword)) {
                return Response.noContent().build();
            } else {
                return Response.serverError().build();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/{user_id}/projects")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUserProjects(@CookieParam("accessTokenCookie") Cookie cookie, @PathParam("user_id") Long userId) {
        User u = userManager.findUserByParam("id", userId);
        if (u == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            if (authorizeUser(cookie, User.UserRole.REGISTERED) && userId.toString().equals(getCookieClaim("userid", cookie))) {
                return Response.ok(createJsonWithView(EntityViews.Private.class, projectManager.findUserProjects(u, false))).build();

            } else {
                return Response.ok(createJsonWithView(EntityViews.Public.class, projectManager.findUserProjects(u, true))).build();
            }
        } catch (JsonProcessingException ex) {
            Logger.getLogger(UserApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }
}
