/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;

import com.foraldom.cf.model.Contribution;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface ContributionManager {
    
    public void createContribution(Contribution c);
    
    public Contribution findContribution(Long contributionId);
    
    public boolean changeContributionState(String orderId, Contribution.ContributionState state);
    
    public Contribution findContribution(String orderId);
    
    public List<Contribution> findProjectContributions(Long projectId);
    
    public List<Contribution> findPaidProjectContributions(Long projectId);
    
    public Long getSuccessProjectContributionsSum(Long projectId);
    
    public void update(Contribution c);
    
}
