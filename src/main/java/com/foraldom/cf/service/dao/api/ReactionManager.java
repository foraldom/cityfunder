/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;


import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.PreexistingEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.Reaction;
import com.foraldom.cf.model.User;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface ReactionManager {
    
    public void create(Reaction reaction) throws PreexistingEntityException, RollbackFailureException, Exception;
    
    public List<Reaction> getProjectReactions(Long projectId, Reaction.ReactionType reactionType);
    
    public Long getProjectReactionsCount(Long projectId, Reaction.ReactionType reactionType);
    
    public boolean remove(Reaction reaction);
    
    public Reaction findReaction(User user, Project project);
    
    public void edit(Reaction reaction) throws NonexistentEntityException, RollbackFailureException, Exception;
    
    public void destroy(Reaction reaction) throws NonexistentEntityException, RollbackFailureException, Exception;
    
}
