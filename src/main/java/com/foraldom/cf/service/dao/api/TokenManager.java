/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.foraldom.cf.model.Token;
import com.foraldom.cf.model.User;
import java.util.Date;
import javax.ws.rs.core.NewCookie;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface TokenManager {
    
    public static String ACCESS_TOKEN_COOKIE_NAME = "accessTokenCookie";
    public static String REFRESH_TOKEN_COOKIE_NAME = "refreshTokenCookie";
    
    public void create(Token token);
    
    public boolean isValid(String tokenId);
    
    public Token find(String tokenId);
    
    public String createJWT(Date expires, User u);
    
    public DecodedJWT decodeJWT(String JWT);
    
    public void createFromCookie(NewCookie tokenCookie, User u);
    
    public void delete(String tokenId);
}
