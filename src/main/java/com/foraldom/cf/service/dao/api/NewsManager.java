/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;


import com.foraldom.cf.service.dao.exceptions.PreexistingEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.News;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface NewsManager {
    
    public News findNews(Long id);
    
    public void create(News news) throws PreexistingEntityException, RollbackFailureException, Exception;
    
    public List<News> getProjectNews(long projectId);
    
}
