package com.foraldom.cf.service.dao.api;

import com.foraldom.cf.model.User;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface UserManager {
    
    public boolean registerUser(User user);
    
    public User findUser(String email);
    
    public boolean verifyUser(String email, String password);
    
    public User findUserByParam(String paramName, Object paramValue);
    
    public boolean updateUser(User u);
    
    public boolean updateUserPsswd(User u, String newPassword);
    
}
