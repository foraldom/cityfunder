/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;

import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Permission;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface ProjectManager {
    
    public void create(Project project) throws RollbackFailureException, Exception;
    public List<Project> findProjectEntities();
    public List<Project> findPublicProjects();
    public List<Project> findProjectsByState(String state);
    public List<Project> findUserProjects(User u, boolean onlyPublic);
    public Project findProject(Long id);
    public void edit(Project project) throws NonexistentEntityException, RollbackFailureException, Exception;    
}
