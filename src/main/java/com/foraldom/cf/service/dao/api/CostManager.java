/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;

import com.foraldom.cf.service.dao.exceptions.NonexistentEntityException;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Cost;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface CostManager {
    
    public void create(Cost cost) throws RollbackFailureException, Exception;
    
    public List<Cost> getProjectCosts(long projectId);
    
    public void edit(Cost cost) throws NonexistentEntityException, com.foraldom.cf.service.dao.exceptions.RollbackFailureException, Exception;
    
    public void destroy(Cost c) throws RollbackFailureException, Exception;
    
    public Cost findCost(Long costId);
    
    public void updateProjectProgress(Long projectId);
    
}
