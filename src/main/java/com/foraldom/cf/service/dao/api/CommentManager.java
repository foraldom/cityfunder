/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.dao.api;


import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Comment;
import java.util.List;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface CommentManager {
    
    public void create(Comment comment) throws RollbackFailureException, Exception;
    
    public List<Comment> getProjectComments(long projectId);
    
    public Comment findComment(Long commentId);

    public void edit(Comment comment);

    public void destroy(Comment c) throws RollbackFailureException, Exception;
    
}
