/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import com.foraldom.cf.service.dao.api.TokenManager;
import com.foraldom.cf.service.dao.api.UserManager;
import com.foraldom.cf.model.Token;
import com.foraldom.cf.model.User;
import com.foraldom.cf.service.AbstractApi;
import com.foraldom.cf.utils.impl.EntityViews;
import java.security.SecureRandom;
import java.util.Date;
import javax.json.Json;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response.Status;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Path("/auth")
public class AuthApi extends AbstractApi{

    private final Integer ACCESS_TOKEN_TTL = 3600; // seconds
    private final Integer REFRESH_TOKEN_TTL = 432000; // seconds

    @Inject
    TokenManager tokenDAO;

    @Inject
    UserManager userMgr;

    @POST
    @Path("/login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response authorize(User user) {
        String email = user.getEmail();
        String password = user.getPassword();

        if (email == null || password == null) {
            // invalid request
            return Response.status(Status.BAD_REQUEST).build();
        }

        if (!userMgr.verifyUser(email, password)) {
            // invalid credentials
            return Response.status(Status.UNAUTHORIZED).entity("user " + email + " not verified").build();
        }

        User u = userMgr.findUser(email);

        NewCookie accessTokenCookie;
        
        NewCookie refreshTokenCookie = createRefreshTokenCookie();

        tokenDAO.createFromCookie(refreshTokenCookie, u);

        accessTokenCookie = createAccessTokenCookie(u);

        try {
            return Response.status(Status.OK)
                    .entity(createJsonWithView(EntityViews.Public.class, u))
                    .cookie(refreshTokenCookie)
                    .cookie(accessTokenCookie)
                    .build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AuthApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("/refresh")
    public Response refreshToken(@CookieParam("refreshTokenCookie") Cookie refreshCookie) {
        if (refreshCookie == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }
        Token foundToken = tokenDAO.find(refreshCookie.getValue());

        if (foundToken != null && foundToken.isValid()) {
            try {
                String email = foundToken.getOwner().getEmail();
                User u = userMgr.findUser(email);
                if (u == null) {
                    return Response.status(Status.UNAUTHORIZED).build();
                }
                NewCookie newAT = createAccessTokenCookie(u);
                NewCookie newRT = createRefreshTokenCookie();
                tokenDAO.delete(foundToken.getId());
                tokenDAO.createFromCookie(newRT, u);
                return Response
                        .noContent()
                        .cookie(newAT)
                        .cookie(newRT)
                        .build();
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(AuthApi.class.getName()).log(Level.SEVERE, null, ex);
                return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
            }
        } else {
            return Response.status(Status.UNAUTHORIZED).build();
        }

    }

    @POST
    @Path("/logout")
    public Response logout(@CookieParam("refreshTokenCookie") Cookie refreshCookie) {
        if (refreshCookie == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }
        Token foundToken = tokenDAO.find(refreshCookie.getValue());
        if (foundToken == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        tokenDAO.delete(foundToken.getId());
        return Response.noContent()
                .header("Set-cookie", "accessTokenCookie=deleted;Domain=.localhost;Path=/;Expires=Thu, 01-Jan-1970 00:00:01 GMT")
                .header("Set-cookie", "refreshTokenCookie=deleted;Domain=.localhost;Path=/;Expires=Thu, 01-Jan-1970 00:00:01 GMT")
                .build();
    }

    private NewCookie createRefreshTokenCookie() {
        Date date = new Date();
        date.setTime(date.getTime() + 1000 * REFRESH_TOKEN_TTL);

        String refreshToken = generateRandomString(30);

        NewCookie refreshTokenCookie = new NewCookie(
                "refreshTokenCookie", // cookie name
                refreshToken, // cookie value
                "/CF-1.0-SNAPSHOT/api/v1/auth", "localhost", // path, domain
                0, // version
                "", // commment
                REFRESH_TOKEN_TTL, // max age
                date, // expiry
                false, // secure - change on HTTPS!!
                true // HttpOnly
        );
        return refreshTokenCookie;
    }

    private NewCookie createAccessTokenCookie(User u) {
        Date date = new Date();
        date.setTime(date.getTime() + 1000 * ACCESS_TOKEN_TTL);
        String accessTokenStr;

        accessTokenStr = tokenDAO.createJWT(date, u);

        NewCookie accessTokenCookie = new NewCookie(
                "accessTokenCookie", // cookie name
                accessTokenStr, // cookie value
                "/", "localhost", // path, domain
                0, // version
                "", // commment
                ACCESS_TOKEN_TTL, // max age
                date, // expiry
                false, // secure - change while on HTTPS!!
                true // HttpOnly
        );
        return accessTokenCookie;
    }

    private String generateRandomString(int lenght) {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[lenght];
        random.nextBytes(bytes);
        byte[] encoded = Base64.encode(bytes);
        String randomString = new String(encoded);
        return randomString;
    }
}
