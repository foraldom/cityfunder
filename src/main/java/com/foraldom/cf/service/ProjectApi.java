/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.foraldom.cf.service.dao.api.CommentManager;
import com.foraldom.cf.service.dao.api.ContributionManager;
import com.foraldom.cf.service.dao.api.CostManager;
import com.foraldom.cf.service.dao.api.NewsManager;
import com.foraldom.cf.service.dao.api.ProjectManager;
import com.foraldom.cf.service.dao.api.ReactionManager;
import com.foraldom.cf.service.dao.api.UserManager;
import com.foraldom.cf.service.dao.exceptions.RollbackFailureException;
import com.foraldom.cf.model.Comment;
import com.foraldom.cf.model.Contribution;
import com.foraldom.cf.model.Contribution.ContributionState;
import com.foraldom.cf.model.Cost;
import com.foraldom.cf.model.News;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.Project.ProjectState;
import com.foraldom.cf.model.Reaction;
import com.foraldom.cf.model.User;
import com.foraldom.cf.service.payments.api.PaymentsManager;
import com.foraldom.cf.utils.api.PATCH;
import com.foraldom.cf.utils.impl.EntityViews;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.bouncycastle.asn1.ocsp.ResponderID;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Path("projects")
public class ProjectApi extends AbstractApi {

    public static final String PROJECTS_URL_PART = Project.URL_PART;
    public static final String COMMENTS_URL_PART = Comment.URL_PART;
    public static final String COSTS_URL_PART = Cost.URL_PART;
    public static final String REACTIONS_URL_PART = Reaction.URL_PART;
    public static final String NEWS_URL_PART = News.URL_PART;
    public static final String CONTRIBUTIONS_URL_PART = Contribution.URL_PART;

    @Inject
    ProjectManager projectManager;

    @Inject
    CostManager costManager;

    @Inject
    UserManager userManager;

    @Inject
    NewsManager newsManager;

    @Inject
    CommentManager commentManager;

    @Inject
    ReactionManager reactionManager;

    @Inject
    PaymentsManager paymentManager;

    @Inject
    ContributionManager contributionManager;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createProject(Project newProject, @CookieParam("accessTokenCookie") Cookie cookie) {
        if (!super.authorizeUser(cookie, User.UserRole.REGISTERED)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        try {
            User u = userManager.findUserByParam("id",getUserId(cookie));
            if (u == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity("user doesnt exists").build();
            }
            newProject.setAuthor(u);
            projectManager.create(newProject);
            return Response.created(new URI(PROJECTS_URL_PART + "/" + newProject.getId())).build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPublicProjects(@QueryParam("state") String projectState, @CookieParam("accessTokenCookie") Cookie cookie) {
        if (Project.isNonPublicProjectState(projectState)) {
            if (super.authorizeUser(cookie, User.UserRole.CITY_ADMIN)) {
                try {
                    return Response.ok(createJsonWithView(EntityViews.Public.class, projectManager.findProjectsByState(projectState))).build();
                } catch (JsonProcessingException ex) {
                    Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        }
        try {
            if (projectState == null) {
                return Response.ok(createJsonWithView(EntityViews.Public.class, projectManager.findPublicProjects())).build();
            } else {
                return Response.ok(createJsonWithView(EntityViews.Public.class, projectManager.findProjectsByState(projectState))).build();
            }
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{projectId}")
    public Response getProject(@PathParam("projectId") Long id, @CookieParam("accessTokenCookie") Cookie cookie) {
        Project p = projectManager.findProject(id);
        if(p == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (p.getProjectState().equals(Project.ProjectState.NEW) || p.getProjectState().equals(Project.ProjectState.CANCELED)) {
            // if user is not author or city admin
            if (!authorizeUser(cookie, User.UserRole.CITY_ADMIN)
                    && (!authorizeUser(cookie, User.UserRole.REGISTERED)
                    || !p.getAuthor().getId().equals(getUserId(cookie)))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        }
        try {
            return Response.ok(createJsonWithView(EntityViews.Public.class, p)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @PATCH
    @Path("/{projectId}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateProject(Project project, @CookieParam("accessTokenCookie") Cookie cookie, @PathParam("projectId") Long projectId) throws JsonProcessingException {
        Project p = projectManager.findProject(projectId);
        if (p == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        // is author
        if (authorizeUser(cookie, User.UserRole.REGISTERED) && p.getAuthor().getId().equals(getUserId(cookie))) {
            if (project.getAuthor() == null && project.getId() == null && p.getProjectState() == ProjectState.NEW && project.getProjectState() == ProjectState.NEW) {
                try {
                    project.setProjectState(null);  // do not change state
                    project.setId(p.getId());
                    projectManager.edit(project);
                    return Response.noContent().build();
                } catch (Exception ex) {
                    Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
                    return Response.serverError().build();
                }
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).entity("Cannot change author, state, id in phase NEW or anything when project state is not in phase NEW").build();
            }
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @PUT
    @Path("{project_id}/state")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response changeProjectState(@CookieParam("accessTokenCookie") Cookie cookie, @PathParam("project_id") Long projectId, ProjectState state) {
        if (state == null) {
            // no changes
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Project p = projectManager.findProject(projectId);
        if (p == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (authorizeUser(cookie, User.UserRole.REGISTERED) && getUserId(cookie).equals(p.getAuthor().getId())) {
            // author can change state to FOR APPROVAL
            if (state.equals(ProjectState.FOR_APPROVAL)) {
                try {
                    p.putForApproval();
                    projectManager.edit(p);
                    return Response.noContent().build();
                } catch (com.foraldom.cf.service.dao.exceptions.RollbackFailureException ex) {
                    Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } // cityadmin can change to every state
        else if (authorizeUser(cookie, User.UserRole.CITY_ADMIN)) {
            p.setProjectState(state);
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        try {
            projectManager.edit(p);
            return Response.noContent().build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    // CONTRIBUTIONS
    @POST
    @Path("{project_id}/" + CONTRIBUTIONS_URL_PART)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response contributeToProject(@Context HttpServletRequest request, @PathParam("project_id") Long projectId, @CookieParam("accessTokenCookie") Cookie cookie, HashMap contributionValues) {
        Project p = projectManager.findProject(projectId);
        if (p == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!p.getProjectState().equals(Project.ProjectState.IN_FUNDING)) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Cannot contribute to project which is not in funding phase.").build();
        }
        User u = null;
        if (cookie != null) {
            u = userManager.findUserByParam("id",getUserId(cookie));
        }
        try {
            return paymentManager.createPayment(Integer.parseInt(contributionValues.get("contributedValue").toString()), p, request.getRemoteAddr(), u);
        } catch (NumberFormatException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path(CONTRIBUTIONS_URL_PART)
    public Response recieveNotification(@Context HttpServletRequest request) {
        if (paymentManager.recieveOrderNotification(request)) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{projectId}/" + CONTRIBUTIONS_URL_PART)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPaidContributions(@PathParam("projectId") Long projectId) {
        Project p = projectManager.findProject(projectId);
        if (p == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            return Response.ok(createJsonWithView(EntityViews.Public.class, contributionManager.findPaidProjectContributions(projectId))).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{projectId}/" + CONTRIBUTIONS_URL_PART + "/{contributionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getContribution(@PathParam("contributionId") Long contributionId, @CookieParam("accessTokenCookie") Cookie cookie) {
        Contribution c = contributionManager.findContribution(contributionId);
        try {
            if (c.getContributionState().equals(ContributionState.PAID)) {
                return Response.ok(createJsonWithView(EntityViews.Public.class, c)).build();

            } else if (authorizeUser(cookie, User.UserRole.CITY_ADMIN)) {
                return Response.ok(createJsonWithView(EntityViews.Public.class, c)).build();
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    // COSTS 
    @GET
    @Path("{project_id}/" + COSTS_URL_PART)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectCosts(@PathParam("project_id") long projectId) {
        try {
            List<Cost> costs = costManager.getProjectCosts(projectId);
            return Response.ok(createJsonWithView(EntityViews.Public.class, costs)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("{project_id}/" + COSTS_URL_PART)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createProjectCost(Cost cost, @PathParam("project_id") Long projectId, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!super.authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            if (!p.getProjectState().equals(Project.ProjectState.NEW)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity("Project is not in NEW phase, cannot modify costs.").build();
            }
            if (!p.getAuthor().getId().equals(getUserId(cookie))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            cost.setProject(p);
            costManager.create(cost);
            return Response
                    .created(new URI(PROJECTS_URL_PART + "/" + projectId + "/" + COSTS_URL_PART + "/" + cost.getId()))
                    .entity(createJsonWithView(EntityViews.Public.class, cost))
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("{project_id}/" + COSTS_URL_PART + "/{cost_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectCost(@PathParam("cost_id") Long costId) {
        try {
            Cost c = costManager.findCost(costId);
            if (c == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(createJsonWithView(EntityViews.Public.class, c)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @PATCH
    @Path("{project_id}/" + COSTS_URL_PART + "/{cost_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateCost(Cost cost, @PathParam("project_id") Long projectId, @PathParam("cost_id") Long costId, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!super.authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            if (!p.getProjectState().equals(Project.ProjectState.NEW)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity("Project is not in NEW phase, cannot modify costs.").build();
            }
            if (!p.getAuthor().getId().equals(getUserId(cookie))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            cost.setId(costId);
            costManager.edit(cost);
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
        return Response.noContent().build();
    }

    @DELETE
    @Path("{project_id}/" + COSTS_URL_PART + "/{cost_id}")
    public Response removeProjectCost(@PathParam("project_id") long projectId, @PathParam("cost_id") long costId, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!super.authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            if (!p.getProjectState().equals(Project.ProjectState.NEW)) {
                return Response.status(Response.Status.UNAUTHORIZED).entity("Project is not in NEW phase, cannot modify costs.").build();
            }
            if (!p.getAuthor().getId().equals(getUserId(cookie))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Cost c = costManager.findCost(costId);
            if (c == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            costManager.destroy(c);

        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
        return Response.noContent().build();
    }

    // NEWS
    @GET
    @Path("{project_id}/" + NEWS_URL_PART)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectNews(@PathParam("project_id") long projectId) {
        List<News> news = newsManager.getProjectNews(projectId);
        try {
            return Response.ok(createJsonWithView(EntityViews.Public.class, news)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("{project_id}/" + NEWS_URL_PART)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createProjectNews(@PathParam("project_id") long projectId, News news, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            if (!p.getAuthor().getId().equals(getUserId(cookie))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            news.setCreated(new Date());
            news.setProject(p);
            newsManager.create(news);
            return Response
                    .created(new URI(PROJECTS_URL_PART + "/" + projectId + "/" + NEWS_URL_PART + "/" + news.getId()))
                    .entity(createJsonWithView(EntityViews.Public.class, news))
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("{project_id}/" + NEWS_URL_PART + "/{news_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSingleProjectNews(@PathParam("news_id") long newsId) {
        News news = newsManager.findNews(newsId);
        if (news == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                return Response.ok(createJsonWithView(EntityViews.Public.class, news)).build();
            } catch (JsonProcessingException ex) {
                Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
                return Response.serverError().build();
            }
        }
    }

    // COMMENTS
    @GET
    @Path("{project_id}/" + COMMENTS_URL_PART)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectComments(@PathParam("project_id") long projectId) {
        try {
            List<Comment> news = commentManager.getProjectComments(projectId);
            return Response.ok(createJsonWithView(EntityViews.Public.class, news)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("{project_id}/" + COMMENTS_URL_PART)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createProjectComment(@PathParam("project_id") long projectId, Comment comment, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            User u = userManager.findUserByParam("id",getUserId(cookie));

            comment.setCreated(new Date());
            comment.setProject(p);
            comment.setAuthor(u);
            commentManager.create(comment);
            return Response.created(new URI(PROJECTS_URL_PART + "/" + p.getId() + "/" + COMMENTS_URL_PART + "/" + comment.getId())).build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("{project_id}/" + COMMENTS_URL_PART + "/{comment_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getComment(@PathParam("comment_id") Long commentId) {
        Comment c = commentManager.findComment(commentId);
        if (c == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            return Response.ok(createJsonWithView(EntityViews.Public.class, c)).build();
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @PATCH
    @Path("{project_id}/" + COMMENTS_URL_PART + "/{comment_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateComment(Comment comment, @PathParam("project_id") Long projectId, @PathParam("comment_id") Long commentId, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!super.authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Comment c = commentManager.findComment(commentId);
            if (c == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("comment with id " + projectId + " doesnt exists.").build();
            }
            if (!c.getAuthor().getId().equals(getUserId(cookie))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            comment.setId(commentId);
            commentManager.edit(comment);
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
        return Response.noContent().build();
    }

    @DELETE
    @Path("{projectId}/" + COMMENTS_URL_PART + "/{comment_id}")
    public Response removeComment(@CookieParam("accessTokenCookie") Cookie cookie, @PathParam("comment_id") Long commentId) {
        try {
            Comment c = commentManager.findComment(commentId);
            if (c == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            if (!authorizeUser(cookie, User.UserRole.CITY_ADMIN) && (!authorizeUser(cookie, User.UserRole.REGISTERED) || !c.getAuthor().getId().equals(getUserId(cookie)))) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            commentManager.destroy(c);
            return Response.noContent().build();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.NOT_FOUND).entity("reaction not found").build();
        }

    }

    // REACTIONS
    @GET
    @Path("{projectId}/" + REACTIONS_URL_PART)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectReactions(@PathParam("projectId") long projectId, @QueryParam("type") Reaction.ReactionType reactionType, @QueryParam("count") String count) {
        try {
            if (count != null) {
                return Response.ok(createJsonWithView(EntityViews.Public.class, reactionManager.getProjectReactionsCount(projectId, reactionType))).build();

            } else {
                return Response.ok(createJsonWithView(EntityViews.Public.class, reactionManager.getProjectReactions(projectId, reactionType))).build();
            }
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("{projectId}/" + REACTIONS_URL_PART)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createReaction(@PathParam("projectId") long projectId, Reaction reaction, @CookieParam("accessTokenCookie") Cookie cookie) {
        try {
            if (!authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            User u = userManager.findUserByParam("id",getUserId(cookie));
            if (p == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("project with id " + projectId + " doesnt exists.").build();
            }
            reaction.setProject(p);
            reaction.setUser(u);
            reaction.setReacted(new Date());
            if (reactionManager.findReaction(u, p) != null) {
                reactionManager.edit(reaction);
            } else {
                reactionManager.create(reaction);
            }
            return Response.noContent().build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("{projectId}/" + REACTIONS_URL_PART)
    public Response removeReaction(@CookieParam("accessTokenCookie") Cookie cookie, @PathParam("projectId") Long projectId) {
        try {
            if (!authorizeUser(cookie, User.UserRole.REGISTERED)) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
            Project p = projectManager.findProject(projectId);
            User u = userManager.findUserByParam("id",getUserId(cookie));
            Reaction r = new Reaction();
            r.setProject(p);
            r.setUser(u);
            reactionManager.destroy(r);
            return Response.ok().build();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        } catch (Exception ex) {
            Logger.getLogger(ProjectApi.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.NOT_FOUND).entity("reaction not found").build();
        }

    }

    // PERMISSIONS
}
