/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service.payments.api;

import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public interface PaymentsManager {
    
    public Response createPayment(int price, Project project, String userIp, User u);
    
    public boolean recieveOrderNotification(HttpServletRequest request);
    
    public int getSuccessPaymentsSum(Project project);
    
}
