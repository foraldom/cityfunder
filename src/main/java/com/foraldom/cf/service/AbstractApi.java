/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.service;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.service.dao.api.TokenManager;
import com.foraldom.cf.model.User;
import javax.inject.Inject;
import javax.ws.rs.core.Cookie;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public abstract class AbstractApi {
    
    protected ObjectMapper mapper = new ObjectMapper();
    
    @Inject
    TokenManager tokenMngr;

    protected boolean authorizeUser(Cookie cookie, User.UserRole userRole) {
        try {
            if(cookie == null){
                return false;
            }
            DecodedJWT accessToken = tokenMngr.decodeJWT(cookie.getValue());            
            if (User.UserRole.valueOf(accessToken.getClaim("role").asString()).compareTo(userRole) >= 0) {
                return true;
            }
        } catch (JWTVerificationException ex) {
            return false;
        }
        return false;
    }
    
    protected String getCookieClaim(String claimName, Cookie cookie){
        if(cookie == null){
            return "";
        }
        DecodedJWT accessToken = tokenMngr.decodeJWT(cookie.getValue());      
        return accessToken.getClaim(claimName).asString();
    }
    
    protected Long getUserId(Cookie cookie){
         if(cookie == null){
            return -1L;
        }
        DecodedJWT accessToken = tokenMngr.decodeJWT(cookie.getValue());      
        return accessToken.getClaim("userid").as(Long.class);
    }
    
    protected String createJsonWithView(Class view, Object o) throws JsonProcessingException{
        return mapper.writerWithView(view).writeValueAsString(o);
    }
    
    protected String buildErrorJsonMessage(String errorMessage){
        return "{\"error\" : \"" + errorMessage + "\"}";
    }
}
