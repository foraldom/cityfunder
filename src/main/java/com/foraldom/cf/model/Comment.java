/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.DateFormatter;
import com.foraldom.cf.utils.impl.HtmlEscape;
import com.foraldom.cf.utils.impl.EntityViews;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
public class Comment extends AbstractEntity {

    public static final String URL_PART = "comments";

    private static final long serialVersionUID = 1L;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateFormatter.ISO8601_PATTERN)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    @ManyToOne
    private User author;
    private String content;
    @JsonView(EntityViews.Private.class)
    @ManyToOne
    private Project project;

    public List<ApiLink> getLinks() {
        if (project != null) {
            String projectUrl = "/" + Project.URL_PART + "/" + project.getId() + "/";
            links.add(new ApiLink("self", projectUrl + URL_PART + "/" + getId()));
        }
        return links;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Project getProject() {
        return project;
    }

    public Date getCreated() {
        return created;
    }

    public User getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setContent(String content) {
        this.content = HtmlEscape.escape(content);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.foraldom.cf.model.Comment[ id=" + id + " ]";
    }

}
