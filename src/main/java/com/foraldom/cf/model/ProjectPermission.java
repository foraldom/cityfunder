package com.foraldom.cf.model;

import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
//@AssociationOverrides({
//		@AssociationOverride(name = "id.project",
//			joinColumns = @JoinColumn(name = "project_id")),
//		@AssociationOverride(name = "id.permission",
//			joinColumns = @JoinColumn(name = "permission_id")) })
public class ProjectPermission implements Serializable {

    @EmbeddedId
    private ProjectPermissionId id = new ProjectPermissionId();
    private boolean passed = false;

    public ProjectPermissionId getId() {
        return id;
    }

    public void setId(ProjectPermissionId id) {
        this.id = id;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public boolean isPassed() {
        return passed;
    }
}
