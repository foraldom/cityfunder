/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.HtmlEscape;
import com.foraldom.cf.utils.impl.EntityViews;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
@Table(name = "users")
@XmlRootElement
public class User extends AbstractEntity {

    public static final String URL_PART = "users";

    public enum UserRole {
        REGISTERED, CITY_ADMIN, ADMIN
    }

    private static final long serialVersionUID = 1L;

    @JsonView(EntityViews.Public.class)
    private String name;
    @JsonView(EntityViews.App.class)
    private String password;
    @JsonView(EntityViews.Public.class)
    @Column(unique = true)
    private String email;
    @Enumerated(EnumType.STRING)
    private UserRole userRole;    

    public List<ApiLink> getLinks() {
        links.add(new ApiLink("self", URL_PART + "/" + getId()));
        return links;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = HtmlEscape.escape(email);
    }

    public String getName() {
        return name;
    }

    public void setName(String username) {
        this.name = HtmlEscape.escape(username);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + name + ", password=" + password + ", email=" + email + ", userRole=" + userRole + '}';
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userRole
     */
    public UserRole getUserRole() {
        return userRole;
    }

    /**
     * @param userRole the userRole to set
     */
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

}
