/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.HtmlEscape;
import com.foraldom.cf.utils.impl.EntityViews;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
@XmlRootElement
public class Cost extends AbstractEntity {
    
    public static final String URL_PART = "costs";

    public enum CostType {
        FINANCE, MATERIAL, TIME
    }

    private static final long serialVersionUID = 1L;
    @ManyToOne
    @JoinColumn(name="project_id")
    @JsonView(EntityViews.Private.class)
    private Project project;
    @Enumerated(EnumType.STRING)
    private CostType costType;
    private int costAmount;
    private String title;
    private String description;
    
    public List<ApiLink> getLinks() {
        if (project != null) {
            String projectUrl = "/" + Project.URL_PART + "/" + project.getId() + "/";
            links.add(new ApiLink("self", projectUrl + URL_PART + "/" + getId()));
        }
        return links;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cost)) {
            return false;
        }
        Cost other = (Cost) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cost{" + "id=" + id + ", project=" + project + ", costType=" + costType + ", costAmount=" + costAmount + '}';
    }



    /**
     * @return the project
     */
    @XmlTransient
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the costType
     */
    public CostType getCostType() {
        return costType;
    }

    /**
     * @param costType the costType to set
     */
    public void setCostType(CostType costType) {
        this.costType = costType;
    }

    /**
     * @return the costAmount
     */
    public int getCostAmount() {
        return costAmount;
    }

    /**
     * @param costAmount the costAmount to set
     */
    public void setCostAmount(int costAmount) {
        this.costAmount = costAmount;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = HtmlEscape.escape(title);
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = HtmlEscape.escape(description);
    }

}
