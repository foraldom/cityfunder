/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.DateFormatter;
import com.foraldom.cf.utils.impl.EntityViews;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
public class Reaction implements Serializable {
    
    public static final String URL_PART = "reactions";
    
    public enum ReactionType {
        like, dislike
    }

    private static final long serialVersionUID = 1L;
    @ManyToOne
    @Id
    private User user;
    @JsonView(EntityViews.Private.class)
    @ManyToOne
    @Id
    private Project project;
    @Enumerated(EnumType.STRING)
    private ReactionType reaction;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateFormatter.ISO8601_PATTERN)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date reacted;

    public void setUser(User user) {
        this.user = user;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setReaction(ReactionType reaction) {
        this.reaction = reaction;
    }

    public void setReacted(Date reacted) {
        this.reacted = reacted;
    }
    
    

    public User getUser() {
        return user;
    }

    public Project getProject() {
        return project;
    }

    public ReactionType getReaction() {
        return reaction;
    }

    public Date getReacted() {
        return reacted;
    }
 

}
