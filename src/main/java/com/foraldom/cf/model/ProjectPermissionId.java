/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */


@Embeddable
public class ProjectPermissionId implements Serializable {
    
    @ManyToOne
    private Project project;
    @ManyToOne
    private Permission permission;

    public Project getProject() {
        return project;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.project);
        hash = 61 * hash + Objects.hashCode(this.permission);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjectPermissionId other = (ProjectPermissionId) obj;
        if (!Objects.equals(this.project, other.project)) {
            return false;
        }
        if (!Objects.equals(this.permission, other.permission)) {
            return false;
        }
        return true;
    }
    
    
}
