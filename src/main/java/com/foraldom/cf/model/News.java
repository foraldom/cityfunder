/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.DateFormatter;
import com.foraldom.cf.utils.impl.HtmlEscape;
import com.foraldom.cf.utils.impl.EntityViews;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
public class News extends AbstractEntity {
    
    public static final String URL_PART = "news";

   
    @ManyToOne
    @JsonView(EntityViews.Private.class)
    private Project project;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateFormatter.ISO8601_PATTERN)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    private String content;
    private String heading; 
    
    public List<ApiLink> getLinks() {
        if (project != null) {
            String projectUrl = "/" + Project.URL_PART + "/" + project.getId() + "/";
            links.add(new ApiLink("self", projectUrl + URL_PART + "/" + getId()));
        }
        return links;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }

    public void setContent(String content) {
        this.content = HtmlEscape.escape(content);
    }

    public void setHeading(String heading) {
        this.heading = HtmlEscape.escape(heading);
    }

    public Date getCreated() {
        return created;
    }

    public String getContent() {
        return content;
    }

    public String getHeading() {
        return heading;
    }   

}
