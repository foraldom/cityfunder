/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foraldom.cf.model;

import com.foraldom.cf.service.ProjectApi;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.HtmlEscape;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
@AttributeOverride(name = "id", column = @Column(name = "project_id"))
public class Project extends AbstractEntity {
    
    @Transient
    public static final String URL_PART = "projects";

    public enum ProjectState {
        CANCELED, NEW, FOR_APPROVAL, IN_FUNDING, FUNDED, IN_REALIZATION, FINISHED
    }

    @Embeddable
    public static class Location {

        private String title;
        @Column(nullable = true)
        private double coordinateLat;
        @Column(nullable = true)
        private double coordinateLong;

        public Location() {

        }

        /**
         * @return the title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title the title to set
         */
        public void setTitle(String title) {
            this.title = HtmlEscape.escape(title);
        }

        /**
         * @return the coordinateLat
         */
        public double getCoordinateLat() {
            return coordinateLat;
        }

        /**
         * @param coordinateLat the coordinateLat to set
         */
        public void setCoordinateLat(double coordinateLat) {
            this.coordinateLat = coordinateLat;
        }

        /**
         * @return the coordinateLong
         */
        public double getCoordinateLong() {
            return coordinateLong;
        }

        /**
         * @param coordinateLong the coordinateLong to set
         */
        public void setCoordinateLong(double coordinateLong) {
            this.coordinateLong = coordinateLong;
        }
    }
    
    @Embeddable
    public static class ProjectProgress {
        private Long pledged;
        private Long goal;
        
        /**
         * @return the pledged
         */
        public Long getPledged() {
            return pledged;
        }

        /**
         * @param pledged the pledged to set
         */
        public void setPledged(Long pledged) {
            this.pledged = pledged;
        }

        /**
         * @return the goal
         */
        public Long getGoal() {
            return goal;
        }

        /**
         * @param goal the goal to set
         */
        public void setGoal(Long goal) {
            this.goal = goal;
        }
    }

    private static final long serialVersionUID = 1L;
    private String name;
    private String descriptionShort;
    private String descriptionLong;
    @Enumerated(EnumType.STRING)
    private ProjectState projectState;
    @Embedded
    private Location location;
    @Embedded
    private ProjectProgress progress;
    @ManyToOne
    private User author;
    
    public List<ApiLink> getLinks() {
        links = new ArrayList<>();
        String projectUrl = "/" + ProjectApi.PROJECTS_URL_PART + "/" + getId() + "/";
        links.add(new ApiLink("self", projectUrl.substring(0, projectUrl.length()-1)));
        links.add(new ApiLink("comments", projectUrl  + ProjectApi.COMMENTS_URL_PART));
        links.add(new ApiLink("costs", projectUrl + ProjectApi.COSTS_URL_PART));
        links.add(new ApiLink("contributions", projectUrl + ProjectApi.CONTRIBUTIONS_URL_PART));
        links.add(new ApiLink("reactions", projectUrl + ProjectApi.REACTIONS_URL_PART));
        links.add(new ApiLink("news", projectUrl + ProjectApi.NEWS_URL_PART));
        return links;
    }

    public Project() {
        projectState = ProjectState.NEW; // project state to approve
    }

    public static boolean isNonPublicProjectState(String state) {
        if (state == null) {
            return false;
        }
        return state.equals(Project.ProjectState.FOR_APPROVAL.toString().toLowerCase())
                || state.equals(Project.ProjectState.NEW.toString().toLowerCase())
                || state.equals(Project.ProjectState.CANCELED.toString().toLowerCase());
    }

    public boolean hasPublicState() {
        return isNonPublicProjectState(getProjectState().toString());
    }

    public void setName(String name) {
        this.name = HtmlEscape.escape(name);
    }

    public String getName() {
        return name;
    }

    public void approve() {
        if (projectState == ProjectState.FOR_APPROVAL) {
            projectState = ProjectState.IN_FUNDING;
        }
    }

    public boolean putForApproval() {
        if (projectState.equals(ProjectState.NEW)) {
            projectState = ProjectState.FOR_APPROVAL;
            return true;
        }
        return false;
    }

    public void cancel() {
        projectState = ProjectState.CANCELED;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Project{" + "name=" + name + ", descriptionShort=" + descriptionShort + ", descriptionLong=" + descriptionLong + ", projectState=" + projectState + ", location=" + location + ", progress=" + progress + ", author=" + author + '}';
    }

   

    /**
     * @return the description
     */
    public String getDescriptionShort() {
        return descriptionShort;
    }

    /**
     * @param description the description to set
     */
    public void setDescriptionShort(String description) {
        this.descriptionShort = HtmlEscape.escape(description);
    }

    /**
     * @return the projectState
     */
    public ProjectState getProjectState() {
        return projectState;
    }

    /**
     * @param projectState the projectState to set
     */
    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    /**
     * @return the author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * @return the descriptionLong
     */
    public String getDescriptionLong() {
        return descriptionLong;
    }

    /**
     * @param descriptionLong the descriptionLong to set
     */
    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = HtmlEscape.escape(descriptionLong);
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the progress
     */
    public ProjectProgress getProgress() {
        return progress;
    }

    /**
     * @param progress the progress to set
     */
    public void setProgress(ProjectProgress progress) {
        this.progress = progress;
    }

}
