/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.foraldom.cf.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.foraldom.cf.utils.impl.ApiLink;
import com.foraldom.cf.utils.impl.DateFormatter;
import com.foraldom.cf.utils.impl.EntityViews;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * 
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@Entity
public class Contribution extends AbstractEntity {
    
    public static final String URL_PART = "contributions";

    public enum ContributionState {
        WAITING, PAID, CANCELED
    }
    
    private static final long serialVersionUID = 1L;
    
    private int contribValue;
    @ManyToOne
    private User contributor;
    @JsonView(EntityViews.Private.class)
    @ManyToOne
    private Project contributedProject;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateFormatter.ISO8601_PATTERN)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date contributed;
    @JsonView(EntityViews.Private.class)
    @Column(unique=true)
    private String orderId;
    @JsonView(EntityViews.Private.class)
    @Enumerated(EnumType.STRING)
    private ContributionState contributionState;
    
    public List<ApiLink> getLinks() {
        if (contributedProject != null) {
            String projectUrl = "/" + Project.URL_PART + "/" + contributedProject.getId() + "/";
            links.add(new ApiLink("self", projectUrl + URL_PART + "/" + getId()));
        }
        return links;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contribution)) {
            return false;
        }
        Contribution other = (Contribution) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.foraldom.cf.model.Contribution[ id=" + id + " ]";
    }

    /**
     * @return the contribValue
     */
    public int getContribValue() {
        return contribValue;
    }

    /**
     * @param contribValue the contribValue to set
     */
    public void setContribValue(int contribValue) {
        this.contribValue = contribValue;
    }

    /**
     * @return the contributor
     */
    public User getContributor() {
        return contributor;
    }

    /**
     * @param contributor the contributor to set
     */
    public void setContributor(User contributor) {
        this.contributor = contributor;
    }


    /**
     * @return the contributed
     */
    public Date getContributed() {
        return contributed;
    }

    /**
     * @param contributed the contributed to set
     */
    public void setContributed(Date contributed) {
        this.contributed = contributed;
    }

    /**
     * @return the contributionState
     */
    public ContributionState getContributionState() {
        return contributionState;
    }

    /**
     * @param contributionState the contributionState to set
     */
    public void setContributionState(ContributionState contributionState) {
        this.contributionState = contributionState;
    }

    /**
     * @return the contributedProject
     */
    public Project getContributedProject() {
        return contributedProject;
    }

    /**
     * @param contributedProject the contributedProject to set
     */
    public void setContributedProject(Project contributedProject) {
        this.contributedProject = contributedProject;
    }

}
