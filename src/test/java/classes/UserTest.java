/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.User;
import com.foraldom.cf.utils.impl.HtmlEscape;
import javax.faces.application.ProjectStage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public class UserTest {
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testEnumValues() throws JsonProcessingException {
         assertEquals(1, User.UserRole.ADMIN.compareTo(User.UserRole.CITY_ADMIN));
         assertEquals(1, User.UserRole.CITY_ADMIN.compareTo(User.UserRole.REGISTERED));
         ObjectMapper om = new ObjectMapper();
         System.out.println(om.writeValueAsString(Project.ProjectState.IN_FUNDING));
     }
     
     @Test
     public void testsads(){
         System.out.println("<p>debile</p>");
         String e = HtmlEscape.escape("<p>debile</p>");
         System.out.println(e);
         e = HtmlEscape.escape(e);
         System.out.println(e);
     }
}
