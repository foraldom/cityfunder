/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import com.foraldom.cf.utils.api.PasswordHash;
import com.foraldom.cf.utils.impl.BCryptHash;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public class PsswdHash {
    
    public PsswdHash() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testHash() {
         String psswd = "supersecretpassword";
         PasswordHash ph = new BCryptHash();
         String hashed = ph.createHash(psswd);
         assertTrue(ph.checkPsswd(psswd, hashed));        
     }
}
