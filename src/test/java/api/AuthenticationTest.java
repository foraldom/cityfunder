/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthenticationTest {

    
    private static String accessCookie;
    private static String refreshCookie;
    private static String refreshCookieOld;


    public AuthenticationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testA_login() throws IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/login");
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(TestData.getRegUser()));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        Header[] headers = httpResponse.getHeaders("Set-cookie");
        accessCookie = headers[0].getValue();
        refreshCookie = headers[1].getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testB_refresh() throws UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/refresh");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        Header[] headers = httpResponse.getHeaders("Set-cookie");        
        accessCookie = headers[0].getValue();
        refreshCookie = headers[1].getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testC_logout() throws IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/logout");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        Header[] headers = httpResponse.getHeaders("Set-cookie");
        accessCookie = headers[0].getValue();
        refreshCookieOld = refreshCookie;
        refreshCookie = headers[1].getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testD1_loggedOut() throws IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/refresh");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }
    
    @Test
    public void testD2_tryRefreshAfterLogout() throws IOException{
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/refresh");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookieOld);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);        
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    public void testE_invalidCredentials() throws IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/login");        
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(TestData.getUserJson("badusername", "evenworsepassword")));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    public void testF_invalidRefreshToken() throws IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/refresh");
        request.addHeader("Cookie", accessCookie + ";");
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }
}
