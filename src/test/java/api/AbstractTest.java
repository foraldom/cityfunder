/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public abstract class AbstractTest {
    
    protected static ObjectMapper mapper = new ObjectMapper();

    public static String readStringFromResponse(HttpResponse response) throws IOException {
        byte[] buffer = new byte[1000];
        int readBytes = response.getEntity().getContent().read(buffer);
        return new String(buffer).substring(0, readBytes);
    }
    
    // first is access, second is refresh
    public static Header[] login(String userJson) throws UnsupportedEncodingException, IOException{
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/login");
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(userJson));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        return httpResponse.getHeaders("Set-cookie");
    }

}
