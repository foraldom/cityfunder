/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.model.User;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
public class TestData {

    public static final String TEST_URL = "http://localhost:8080/CF-1.0-SNAPSHOT/api/v1/";

    public static String getRegUser() {
        return getUserJson("tajny@email.cz", "psswd");
    }

    public static String getRegUser2() {
        return getUserJson("emaaailsds", "psswd");
    }

    public static String getCityAdminUser() {
        return getUserJson("emailemail", "psswd");
    }

    public static String getUserJson(String email, String password) {
        ObjectMapper mapper = new ObjectMapper();
        User u = new User();
        u.setEmail(email);
        u.setPassword(password);
        u.setName("Teston Userson");
        try {
            return mapper.writeValueAsString(u);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
