/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.model.Comment;
import com.foraldom.cf.model.Cost;
import com.foraldom.cf.model.News;
import com.foraldom.cf.model.Project;
import com.foraldom.cf.model.Project.Location;
import com.foraldom.cf.model.Project.ProjectState;
import com.foraldom.cf.model.Reaction;
import com.foraldom.cf.model.User;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectTest extends AbstractTest {

    private static String accessCookie;
    private static String refreshCookie;
    private static String adminAccessCookie;
    private static String adminRefreshCookie;

    private static String createdProjectPath;
    private static Project createdProject;
    private static Cost createdCost;
    private static Comment createdComment;
    private static String createNewsPath;

    public ProjectTest() {
    }

    @BeforeClass
    public static void setUpClass() throws UnsupportedEncodingException, IOException {
        Header[] headers = login(TestData.getRegUser());
        accessCookie = headers[0].getValue();
        refreshCookie = headers[1].getValue();

        headers = login(TestData.getCityAdminUser());
        adminAccessCookie = headers[0].getValue();
        adminRefreshCookie = headers[1].getValue();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testA1_createProject() throws IOException {
        Project p = new Project();
        p.setName("test project");
        p.setDescriptionShort("test description ahoj <br>");
        p.setLocation(new Location());
        p.getLocation().setCoordinateLat(123.32);
        p.getLocation().setCoordinateLong(135.43);
        p.getLocation().setTitle("test location");

        HttpPost request = new HttpPost(TestData.TEST_URL + "projects");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json; charset=UTF-8");

        String jsonInString = mapper.writeValueAsString(p);
        request.setEntity(new StringEntity(jsonInString, "UTF-8"));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        createdProjectPath = httpResponse.getFirstHeader("Location").getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_CREATED));
    }

    @Test
    public void testA2_getCreatedProject() throws IOException {
        HttpGet request2 = new HttpGet(createdProjectPath);
        request2.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request2);
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        Project createdP = mapper.readValue(strResponse, Project.class);
        createdProject = createdP;
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testB_createProjectWithoutAT() throws IOException {
        Project p = new Project();
        p.setName("test project");
        p.setDescriptionShort("test description");

        HttpPost request = new HttpPost(TestData.TEST_URL + "projects");
        request.addHeader("Cookie", refreshCookie);
        request.addHeader("Content-type", "application/json");

        String jsonInString = mapper.writeValueAsString(p);
        request.setEntity(new StringEntity(jsonInString));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    public void testC_createdProjectIsNotPublic() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    public void testD1_updateProjectInfo() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPatch request = new HttpPatch(TestData.TEST_URL + "projects/" + createdProject.getId());
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        Project p = new Project();
        p.setName("project name changed");
        request.setEntity(new StringEntity(mapper.writeValueAsString(p)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testD2_checkUpdatedProject() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId());
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String responseStr = readStringFromResponse(httpResponse);
        Project p = mapper.readValue(responseStr, Project.class);
        assertThat("project name changed", equalTo(p.getName()));
    }

    @Test
    public void testD3_putProjectForApproval() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPut request = new HttpPut(TestData.TEST_URL + "projects/" + createdProject.getId() + "/state");
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.setEntity(new StringEntity(mapper.writeValueAsString(ProjectState.FOR_APPROVAL)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testD4_checkUpdatedProjectState() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String responseStr = readStringFromResponse(httpResponse);
        Project p = mapper.readValue(responseStr, Project.class);
        assertThat(Project.ProjectState.FOR_APPROVAL, equalTo(p.getProjectState()));
    }

    @Test
    public void testD5_changeStateToNewByAdmin() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPut request = new HttpPut(TestData.TEST_URL + "projects/" + createdProject.getId() + "/state");
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", adminAccessCookie + ";" + adminRefreshCookie);
        request.setEntity(new StringEntity(mapper.writeValueAsString(ProjectState.NEW)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    // COSTS
    @Test
    public void testE_addCostToCreatedProject() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/" + createdProject.getId() + "/costs");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");
        Cost c = new Cost();
        c.setCostAmount(10);
        c.setCostType(Cost.CostType.FINANCE);
        c.setProject(createdProject);
        request.setEntity(new StringEntity(mapper.writeValueAsString(c)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        createdCost = mapper.readValue(strResponse, Cost.class);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_CREATED));
    }

    @Test
    // todo add cost to project from another author
    public void testF_addCostToWrongProject() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/0/costs");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");
        ObjectMapper mapper = new ObjectMapper();
        Cost c = new Cost();
        c.setCostAmount(10);
        c.setCostType(Cost.CostType.TIME);
        c.setProject(createdProject);
        request.setEntity(new StringEntity(mapper.writeValueAsString(c)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertTrue((httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) || (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void testG_findAddedProjectCost() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/costs");
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));        
        String strResponse = readStringFromResponse(httpResponse);
        Cost[] costs = mapper.readValue(strResponse, Cost[].class);
        boolean costFound = false;
        for (Cost c : costs) {
            costFound = (c.getCostAmount() == 10);
        }
        assertTrue(costFound);
    }

    @Test
    public void testH_updateAddedProjectCost() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPatch request = new HttpPatch(TestData.TEST_URL + "projects/" + createdProject.getId() + "/costs/" + createdCost.getId());
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        Cost c = new Cost();
        c.setCostAmount(100);
        request.setEntity(new StringEntity(mapper.writeValueAsString(c)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testI1_findChangedProjectCost() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/costs");
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        ObjectMapper mapper = new ObjectMapper();
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        Cost[] costs = mapper.readValue(strResponse, Cost[].class);
        boolean costFound = false;
        for (Cost c : costs) {
            costFound = (c.getCostAmount() == 100);
        }
        assertTrue(costFound);
    }

    @Test
    public void testI2_removeProjectCost() throws IOException {
        HttpDelete request = new HttpDelete(TestData.TEST_URL + "projects/" + createdProject.getId() + "/costs/" + createdCost.getId());
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testI3_addProjectCostAndCheckGoal() throws UnsupportedEncodingException, IOException {
        testE_addCostToCreatedProject();
        testE_addCostToCreatedProject();
        HttpGet request = new HttpGet(createdProjectPath);
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String strResponse = readStringFromResponse(httpResponse);
        Project createdP = mapper.readValue(strResponse, Project.class);
        assertThat(createdP.getProgress().getGoal(), equalTo(20L));
    }

    // COMMENTS
    @Test
    public void testJ_addComment() throws JsonProcessingException, UnsupportedEncodingException, IOException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");

        Comment c = new Comment();
        c.setContent("new comment");
        request.setEntity(new StringEntity(mapper.writeValueAsString(c)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_CREATED));
    }

    @Test
    public void testK_findAddedComment() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments");
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        ObjectMapper mapper = new ObjectMapper();
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        Comment[] costs = mapper.readValue(strResponse, Comment[].class);
        boolean commentFound = false;
        for (Comment c : costs) {
            commentFound = (c.getContent().equals("new comment"));
            if (commentFound) {
                createdComment = c;
                break;
            }
        }
        assertTrue(commentFound);
    }

    @Test
    public void testL_updateComment() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPatch request = new HttpPatch(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments/" + createdComment.getId());
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        Comment c = new Comment();
        c.setContent("changed comment content");
        ObjectMapper mapper = new ObjectMapper();
        request.setEntity(new StringEntity(mapper.writeValueAsString(c)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testM_findUpdatedComment() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments/" + createdComment.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        ObjectMapper mapper = new ObjectMapper();
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        Comment comment = mapper.readValue(strResponse, Comment.class);
        assertTrue(comment.getContent().equals("changed comment content"));
    }

    @Test
    public void testN_removeComment() throws IOException {
        HttpDelete request = new HttpDelete(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments/" + createdComment.getId());
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testO_getRemovedComment() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments/" + createdComment.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void testP_addAndRemoveCommentByAdmin() throws IOException {
        testJ_addComment();
        testK_findAddedComment();
        HttpDelete request = new HttpDelete(TestData.TEST_URL + "projects/" + createdProject.getId() + "/comments/" + createdComment.getId());
        request.addHeader("Cookie", adminAccessCookie + ";" + adminRefreshCookie);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testQ_getAdminRemovedComment() throws IOException {
        testO_getRemovedComment();
    }

    @Test
    public void testR1_approveProjectByAdmin() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPut request = new HttpPut(TestData.TEST_URL + "projects/" + createdProject.getId() + "/state");
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", adminAccessCookie + ";" + adminRefreshCookie);
        request.setEntity(new StringEntity(mapper.writeValueAsString(ProjectState.IN_FUNDING)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testR2_approvedProjectIsPublic() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testR3_cantChangeProjectInFundingPhase() throws IOException {
        HttpPatch request = new HttpPatch(TestData.TEST_URL + "projects/" + createdProject.getId());
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        Project p = new Project();
        p.setName("project name changed again");
        request.setEntity(new StringEntity(mapper.writeValueAsString(p)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    public void testS1_addNewsToProject() throws JsonProcessingException, JsonProcessingException, IOException, UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/" + createdProject.getId() + "/news");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");

        News n = new News();
        n.setContent("test news content");
        n.setHeading("test news heading");
        request.setEntity(new StringEntity(mapper.writeValueAsString(n)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        createNewsPath = httpResponse.getFirstHeader("Location").getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_CREATED));
    }

    @Test
    public void testS2_getAddedNews() throws IOException {
        HttpGet request = new HttpGet(createNewsPath);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String response = readStringFromResponse(httpResponse);
        News n = mapper.readValue(response, News.class);
        assertThat("test news content", equalTo(n.getContent()));
    }

    @Test
    public void testT1_contributeToProject() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HashMap<String, String> map = new HashMap();
        map.put("contributedValue", "1000");
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/" + createdProject.getId() + "/contributions");
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(mapper.writeValueAsString(map)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_MOVED_TEMPORARILY)); // found status
    }

    @Test
    public void testT2_PaymentNotification() throws Exception {
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/contributions");
        request.addHeader("OpenPayu-Signature:", "sender=checkout;signature=577d499345fbae6500fad57a2f9cd928;algorithm=MD5;content=DOCUMENT");
        request.addHeader("X-OpenPayU-Signature", "sender=checkout;signature=577d499345fbae6500fad57a2f9cd928;algorithm=MD5;content=DOCUMENT");
        request.addHeader("PayU-Processing-Time", "1000");
        request.setEntity(new StringEntity("{"
                + "   \"order\":{"
                + "      \"orderId\":\"9QGM1F41NX170411GUEST000P01\","
                + "      \"extOrderId\":\"Order id in your shop\","
                + "      \"orderCreateDate\":\"2012-12-31T12:00:00\","
                + "      \"notifyUrl\":\"http://tempuri.org/notify\","
                + "      \"customerIp\":\"127.0.0.1\","
                + "      \"merchantPosId\":\"{POS ID (pos_id)}\","
                + "      \"description\":\"My order description\","
                + "      \"currencyCode\":\"PLN\","
                + "      \"totalAmount\":\"200\","
                + "      \"buyer\":{"
                + "         \"email\":\"john.doe@example.org\","
                + "         \"phone\":\"111111111\","
                + "         \"firstName\":\"John\","
                + "         \"lastName\":\"Doe\","
                + "         \"language\":\"en\""
                + "      },"
                + "      \"products\":["
                + "         {"
                + "            \"name\":\"Product 1\","
                + "            \"unitPrice\":\"200\","
                + "            \"quantity\":\"1\""
                + "         }"
                + "      ],"
                + "      \"status\":\"COMPLETED\""
                + "      },"
                + "   \"localReceiptDateTime\": \"2016-03-02T12:58:14.828+01:00\","
                + "   \"properties\": ["
                + "         {"
                + "            \"name\": \"PAYMENT_ID\","
                + "            \"value\": \"151471228\""
                + "         }"
                + "      ]"
                + "   }     "));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testT3_checkProjectPledgedValue() throws IOException {
        HttpGet request2 = new HttpGet(createdProjectPath);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request2);
        String strResponse = readStringFromResponse(httpResponse);
        Project createdP = mapper.readValue(strResponse, Project.class);
        assertThat(createdP.getProgress().getPledged(), equalTo(1000L));
    }

    @Test
    public void testU1_addReactionToProject() throws JsonProcessingException, UnsupportedEncodingException, IOException{
        HttpPost request = new HttpPost(TestData.TEST_URL + "projects/" + createdProject.getId() + "/reactions");
        Reaction reaction = new Reaction();
        reaction.setReaction(Reaction.ReactionType.like);
        request.addHeader("Cookie", accessCookie + ";" + refreshCookie);
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(mapper.writeValueAsString(reaction)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT)); 
    }
    
    @Test
    public void testU2_getReactionsCount() throws IOException{
        HttpGet request2 = new HttpGet(TestData.TEST_URL + "projects/" + createdProject.getId() + "/reactions?type=like&count");
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request2);
        String strResponse = readStringFromResponse(httpResponse);
        System.out.println(strResponse);
        assertThat(strResponse, equalTo("1")); 
    }

    // PERMISSIONS    
}
