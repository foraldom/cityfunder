/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foraldom.cf.model.User;
import com.foraldom.cf.service.UserApi;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bouncycastle.util.encoders.Base64;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Dominik Foral foraldom@fit.cvut.cz
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest {

    private static String username;
    private static User regUser;
    private static String at;
    private static String rt;

    public UserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[10];
        random.nextBytes(bytes);
        byte[] encoded = Base64.encode(bytes);
        username = new String(encoded);
        
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testA1_regUser() throws UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "users");
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(TestData.getUserJson(username, "psswd")));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);        
        ObjectMapper mapper = new ObjectMapper();
        User createdU = mapper.readValue(strResponse, User.class);
        regUser = createdU;
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_CREATED));
    }

    @Test
    public void testB_regSameUserAgain() throws UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "users");
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(TestData.getUserJson(username, "psswd")));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_BAD_REQUEST));
    }

    @Test
    public void testC_getRegUser() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "users/" + regUser.getId());
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testD_loginUser() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPost request = new HttpPost(TestData.TEST_URL + "auth/login");
        request.addHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(TestData.getUserJson(regUser.getEmail(), "psswd")));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        Header[] headers = httpResponse.getHeaders("Set-cookie");
        at = headers[0].getValue();
        rt = headers[1].getValue();
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    public void testE_updateUser() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPatch request = new HttpPatch(TestData.TEST_URL + "users/" + regUser.getId());
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", at + ";" + rt);
        User u = new User();
        u.setName("Changed Name");
        ObjectMapper mapper = new ObjectMapper();
        request.setEntity(new StringEntity(mapper.writeValueAsString(u)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    public void testF_checkUpdatedUser() throws IOException {
        HttpGet request = new HttpGet(TestData.TEST_URL + "users/current");
        request.addHeader("Cookie", at + ";" + rt);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        ObjectMapper mapper = new ObjectMapper();
        User createdU = mapper.readValue(strResponse, User.class);
        assertTrue(createdU.getName().equals("Changed Name"));
    }

    @Test
    public void testG_changePassword() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPut request = new HttpPut(TestData.TEST_URL + "users/" + regUser.getId() + "/password");
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", at + ";" + rt);
        UserApi.NewPsswdRequest req = new UserApi.NewPsswdRequest();
        req.newPassword = "changedpsswd";
        req.oldPassword = "psswd";
        ObjectMapper mapper = new ObjectMapper();
        request.setEntity(new StringEntity(mapper.writeValueAsString(req)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }
    
    @Test
    public void testH_changePasswordAgain() throws JsonProcessingException, UnsupportedEncodingException, IOException {
        HttpPut request = new HttpPut(TestData.TEST_URL + "users/" + regUser.getId() + "/password");
        request.addHeader("Content-type", "application/json");
        request.addHeader("Cookie", at + ";" + rt);
        UserApi.NewPsswdRequest req = new UserApi.NewPsswdRequest();
        req.newPassword = "changedpsswd";
        req.oldPassword = "psswd";
        ObjectMapper mapper = new ObjectMapper();
        request.setEntity(new StringEntity(mapper.writeValueAsString(req)));
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }
    
    @Test
    public void testI_getCurrentUser() throws IOException{
        HttpGet request = new HttpGet(TestData.TEST_URL + "users/current");
        request.addHeader("Cookie", at + ";" + rt);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        byte[] response = new byte[1000];
        httpResponse.getEntity().getContent().read(response);
        String strResponse = new String(response);
        ObjectMapper mapper = new ObjectMapper();
        User createdU = mapper.readValue(strResponse, User.class);
        assertTrue(createdU.getId().equals(regUser.getId()));
    }
}
